/*
 * Simple OpenMP RTEMS configuration
 */

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_UNIFIED_WORK_AREAS

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#define CONFIGURE_INIT

#define CONFIGURE_MAXIMUM_PROCESSORS 4

#include <rtems/confdefs.h>

// openmp support
#include <omp.h>

void __attribute__((__constructor__(1000))) config_libgomp(void)
{
    setenv("OMP_DISPLAY_ENV", "VERBOSE", 1);
    setenv("GOMP_SPINCOUNT", "30000", 1);
    setenv("GOMP_DEBUG", "1", 1);
    setenv("OMP_NUM_THREADS", "4",1);
}


