## Build instructions

To build the example you need to download the waf build system with

```bash
curl https://waf.io/waf-2.0.19 > waf
chmod +x waf
```

Then you also need to initialise a new Git repository

```bash 
git init
```

and add RTEMS Waf support as a Git sub-module and initialise it:

```bash
git submodule add git://git.rtems.org/rtems_waf.git rtems_waf
```

Now you can configure und run the build with

```bash
./waf configure --rtems=/workspace/rtems/6 rtems-bsp=$arch/$bsp  # for example riscv/rv64imafdc 
./waf
```

Executable lies in the ./build folder.
