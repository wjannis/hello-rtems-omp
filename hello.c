// OpenMP program to print Hello World
// using C language
 
// the following lines can be deleted, they give back if smp was built correctly
#ifdef RTEMS_SMP
#warning "SMP support is enabled"
#else
#warning "SMP support is disabled"
#endif


// OpenMP header (moved to init.c)
//#include <omp.h>
 
/*
void __attribute__((__constructor__(1000))) config_libgomp(void)
{
    setenv("OMP_DISPLAY_ENV", "VERBOSE", 1);
    setenv("GOMP_SPINCOUNT", "30000", 1);
    setenv("GOMP_DEBUG", "1", 1);
    setenv("OMP_NUM_THREADS", "4",1);
}
*/

#include <stdio.h>
#include <stdlib.h>
#include <rtems.h>
#include <rtems/score/smp.h>

rtems_task Init(
  rtems_task_argument ignored
)
{
 
    // Beginning of parallel region
    #pragma omp parallel
    {
	        printf("Hello World... from thread = %d on CPU %d\n",
               omp_get_thread_num(), _SMP_Get_current_processor());
    }
    // Ending of parallel region
    exit( 0 );
}


